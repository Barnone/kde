# Copyright 2013 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qmake [ slot=5 ]
require python [ blacklist=2 multibuild=false with_opt=true ]

MY_PN="${PN}_src"

SUMMARY="QScintilla is a port to Qt of the Scintilla editing component."
HOMEPAGE="http://www.riverbankcomputing.com/software/qscintilla/"
DOWNLOADS="https://www.riverbankcomputing.com/static/Downloads/${PN}/${PV}/${MY_PN}-${PV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    python [[ description = [ Build the Python bindings for QScintilla ] ]]
"

DEPENDENCIES="
    build:
        python? ( dev-python/PyQt-builder[>=1.6][python_abis:*(-)?] )
    build+run:
        x11-libs/qtbase:5[>=5.11.0]
        python? (
            dev-python/PyQt-builder[>=1.6][python_abis:*(-)?]
            dev-python/PyQt5[>=5.15.1-r1][python_abis:*(-)?]
            dev-python/sip[>=6.0.2][python_abis:*(-)?]
        )
"

WORK="${WORKBASE}"/${MY_PN}-${PV}
EQMAKE_SOURCES="${WORK}"/src/qscintilla.pro

src_compile() {
    default

    if option python; then
        edo pushd Python
        edo mv pyproject-qt5.toml pyproject.toml
        edo ${PYTHON} /usr/$(exhost --target)/bin/sip-build \
            --no-make \
            --qmake /usr/$(exhost --target)/bin/qmake-qt5 \
            --qsci-features-dir src/features \
            --qsci-include-dir src \
            --qsci-library-dir src
            --verbose
        edo cd build
        emake
        edo popd
    fi
}

src_install() {
    default

    if option python; then
        edo cd Python/build
        emake -j1 INSTALL_ROOT="${IMAGE}" install
    fi
}

