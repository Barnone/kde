# Copyright 2016-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="Libraries for the handling of messages"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
"

KF5_MIN_VER=5.79.0
QT_MIN_VER=5.14.0

DEPENDENCIES="
    build:
        virtual/pkg-config   [[ note = [ FindPoppler.cmake ] ]]
    build+run:
        app-crypt/gpgme[qt5][>=1.8.0]
        app-crypt/qca:2[>=2.3.0]
        dev-libs/grantlee:5[>=5.2]
        kde/grantleetheme[>=${PV}]
        kde/libgravatar[>=${PV}]
        kde/libkdepim[>=${PV}]
        kde/libkleo[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/akonadi-search:5[>=${PV}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcalendarcore:5[>=5.63.0]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kldap:5[>=${PV}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kmbox:5[>=${PV}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        kde-frameworks/syntax-highlighting:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
    test:
        app-crypt/gnupg
"

# 49 of 49 tests seem to need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DKDEPIM_ENTERPRISE_BUILD:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DKDEPIM_RUN_AKONADI_TEST:BOOL=TRUE -DKDEPIM_RUN_AKONADI_TEST:BOOL=FALSE'
)

