# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require gtk-icon-cache

export_exlib_phases src_prepare pkg_postinst

SUMMARY="An easy to use, but flexible, composited Window Manager"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 GPL-2"
SLOT="4"
MYOPTIONS="
    accessibility [[ description = [ Enables accessibility features via QAccessibilityClient ] ]]
    eglstream [[ description = [ Build a EGLStream based DRM backend for NVIDIA users ] ]]
    lcms

    ( providers: eudev systemd ) [[
        *description = [ udev provider ]
        number-selected = exactly-one
    ]]
"

if ever at_least 5.22.1 ; then
    KF5_MIN_VER=5.82.0
else
    KF5_MIN_VER=5.78.0
fi
QT_MIN_VER=5.15.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        dev-libs/libepoxy
        kde/breeze:${SLOT}[>=5.9.0]   [[ note = [ possibly optional, default decoration ] ]]
        kde/kdecoration:${SLOT}[>=5.18.0]
        kde/kwayland-server[>=5.21]
        kde/kscreenlocker
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:5[>=${KF5_MIN_VER}]
        kde-frameworks/kinit:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/krunner:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        media/pipewire[>=0.3] [[ note = [ automagic ] ]]
        media-libs/fontconfig
        media-libs/freetype:2
        sys-libs/libcap
        sys-libs/libinput[>=1.9]
        sys-libs/wayland[>=1.2]
        x11-dri/libdrm[>=2.4.62]
        x11-dri/mesa[wayland]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libxcb[>=1.10]
        x11-libs/libXi
        x11-libs/libxkbcommon[>=0.7.0]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qttools:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        x11-utils/xcb-util-cursor
        x11-utils/xcb-util-image
        x11-utils/xcb-util-keysyms
        x11-utils/xcb-util-wm[>=0.4]
        accessibility? ( dev-libs/libqaccessibilityclient )
        lcms? ( media-libs/lcms2 )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        kde-frameworks/kded:5
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-dri/mesa
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
    test:
        sys-libs/wayland-protocols[>=1.19]
        x11-libs/qtwayland:5
    post:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
    suggestion:
        x11-libs/qtmultimedia:5[>=5.3.0] [[ description = [ Preview kwin effects as video ] ]]
        x11-libs/qtvirtualkeyboard:5 [[
            description = [ Integrate a virtual keyboard with kwin ]
        ]]
        x11-server/xorg-server[xwayland] [[ description = [ Needed for running kwin_wayland ] ]]
"

if ever at_least 5.21.90 ; then
    :
else
    DEPENDENCIES+="
        build+run:
            x11-libs/qtscript:5[>=${QT_MIN_VER}]
    "
fi

# 7 of 9 tests require a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DKWIN_BUILD_ACTIVITIES:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'eglstream KWIN_BUILD_EGL_STREAM_BACKEND'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'accessibility QAccessibilityClient'
    'lcms lcms2'
)

kwin_src_prepare() {
    kde_src_prepare

    # Work around sydbox hanging after src_configure if
    # x11-libs/qtmultimedia[pulseaudio] is installed
    edo sed -e "/ecm_find_qmlmodule(QtMultimedia 5.0)/d" \
        -i CMakeLists.txt
}

kwin_pkg_postinst() {
    # allow kwin_wayland to set real-time scheduling policies
    # doesn't carry over from src_install so we need to set this manually
    edo setcap cap_sys_nice+ep /usr/$(exhost --target)/bin/kwin_wayland
}

