# Copyright 2013,2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Common code and data for many KDE games"

LICENCES="GPL-2 FDL-1.2
    GPL-3           [[ note = [ penguins carddeck ] ]]
    CC-PublicDomain [[ note = [ nicu-{ornamental,white} carddeck ] ]]
    MIT             [[ note = [ konqi-modern carddeck ] ]]
"
SLOT=5
MYOPTIONS=""

KF5_MIN_VER="5.68.0"
QT_MIN_VER="5.12.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdnssd:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        (
            media-libs/libsndfile[>=0.21]
            media-libs/openal
        ) [[ note = [ could both be optional and phonon could be used as a fallback, but upstream
                      doesn't recommend it ] ]]
"

libkdegames_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

