#!/bin/sh
#
# Based in part upon '10-agent-shutdown.sh' from Gentoo.
#
# This file is executed at Plasma shutdown.

source /etc/conf.d/plasma-workspace.conf

if [ "${GPG_AGENT}" = true ]; then
	gpgconf --kill gpg-agent >/dev/null 2>&1
fi

if [ "${SSH_AGENT}" = true ]; then
	if [ -n "${SSH_AGENT_PID}" ]; then
		eval "$(ssh-agent -s -k)"
	fi
fi
