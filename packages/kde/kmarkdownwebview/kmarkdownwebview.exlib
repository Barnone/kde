# Copyright 2017-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/${PV}/src" ] kde [ translations='ki18n' ]
require freedesktop-desktop

SUMMARY="A Markdown format viewer KParts plugin"
DESCRIPTION="
* Markdown viewer KParts plugin which allows KParts-using applications to
display files in Markdown format in the target format
* A Markdown file KIO thumbnail generator plugin which allows KIO-powered file
managers & dialogs to show thumbnails and previews of files in Markdown format
in the target format (currently only available when building against QtWebKit).
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    qtwebkit [[
        description = [ Use Qt5WebKit instead of Qt5WebEngine to render Markdown content ]
    ]]
"

KF5_MIN_VER="5.25.0"
QT_MIN_VER="5.6.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        qtwebkit? ( x11-libs/qtwebkit:5[>=${QT_MIN_VER}] )
       !qtwebkit? ( x11-libs/qtwebengine:5[>=${QT_MIN_VER}] )
"

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'qtwebkit USE_QTWEBKIT'
)

