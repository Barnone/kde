# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require gtk-icon-cache

SUMMARY="A learning software that helps improving pronunciation skills"

LICENCES="
    ( BSD-2 BSD-3 ) [[ note = [ cmake scripts ] ]]
    FDL-1.2 [[ note = [ documentation ] ]]
    GPL-2
    LGPL-3  [[ note = [ artwork ] ]]"
MYOPTIONS="
    gstreamer    [[ description = [ Build GStreamer sound backend ] ]]
    qtmultimedia [[ description = [ Build QtMultimedia sound backend ] ]]

    ( gstreamer qtmultimedia ) [[ number-selected = at-least-one ]]
"

KF5_MIN_VER="5.64.0"
QT_MIN_VER="5.11.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui][sql]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtxmlpatterns:5[>=${QT_MIN_VER}]
        gstreamer? ( media-libs/qt-gstreamer[>=1.2.0][qt5(+)] )
        qtmultimedia? ( x11-libs/qtmultimedia:5 )
    run:
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'gstreamer GSTREAMER_PLUGIN'
    'qtmultimedia QTMULTIMEDIA_PLUGIN'
)

