# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE EDU: 2D and 3D Graph Calculator"

LICENCES="GPL-2 FDL-1.2 LGPL-2
    BSD-3 [[ note = [ cmake scripts ] ]]
"

MYOPTIONS="
    console-interface [[ description = [ Provide a console interface for kalgebra ] ]]
"

QT_MIN_VER="5.14.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
    build+run:
        kde/analitza:5[>=14.11.80]
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kio:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kxmlgui:5
        x11-dri/glu
        x11-dri/mesa
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtwebengine:5
        console-interface? (
            sys-libs/ncurses
            sys-libs/readline:=
        )
    run:
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
"
# NOTE: kde-frameworks/kirigami:2 is only used on Android

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'console-interface Curses'
    'console-interface Readline'
)

kalgebra_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kalgebra_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

