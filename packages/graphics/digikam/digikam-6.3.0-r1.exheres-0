# Copyright 2008, 2009, 2010, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2010-2011 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2012 Xavier Barrachina <xabarci@doctor.upv.es>
# Copyright 2016 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir=${PN}/${PV} ] kde
require freedesktop-desktop gtk-icon-cache

SUMMARY="An advanced digital photo management application"
HOMEPAGE="https://www.digikam.org/"

LICENCES="
    ADOBE-DNG  [[ note = [ core/libs/dngwriter ] ]]
    BSD-3      [[ note = [ cmake scripts ] ]]
    CeCILL-2.0 [[ note = [ core/libs/dimg/filters/greycstoration/cimg ] ]]
    FDL-1.2    [[ note = [ documentation ] ]]
    GPL-2 LGPL-2.1
    (
        || ( LGPL-2.1 CDDL-1.0 )
    ) [[ note = [ libraw ] ]]
    (
        GPL-2 GPL-3
    ) [[ note = [ libraw's demosaic files ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    calendar            [[ description = [ Calendar support ] ]]
    contacts            [[ description = [ Access kdepim/kmail contacts ] requires = calendar ]]
    doc
    geolocation         [[ description = [ Map images to locations ] ]]
    gphoto2             [[ description = [ Support for PTP access ] ]]
    jpeg2000
    media-player        [[ description = [ Play non-still media using qtmultimedia ] ]]
    mysql               [[ description = [ Use mysql instead of sqlite for the database ] ]]
    scanner             [[ description = [ Aquire images from scanners via sane ] ]]
    semantic-desktop    [[ description = [ Index files using kfilemetadata ] ]]

    ( linguas:
        af ar az be bg bn br bs ca cs csb cy da de el en_GB eo es et eu fa fi
        fo fr fy ga gl ha he hi hr hsb hu id is it ja ka kk km ko ku lb lo lt
        lv mi mk mn ms mt nb nds ne nl nn nso oc pa pl pt pt_BR ro ru rw se sk
        sl sq sr sr@Latn ss sv ta te tg th tr tt uk uz uz@cyrillic ven vi wa
        xh zh_CN zh_HK zh_TW zu
    )
    ( providers: graphicsmagick imagemagick ) [[
        *description = [ Software suites to create, edit, and compose bitmap images ]
        number-selected = at-least-one
    ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

KF5_MIN_VERSION=5.1.0
QT5_MIN_VERSION=5.6.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VERSION}]
        sys-devel/bison
        sys-devel/flex[>=2.6.0-r1]
        sys-devel/gettext
        virtual/pkg-config
        doc? ( app-doc/doxygen[>=1.8.0] )
    build+run:
        dev-libs/boost
        dev-libs/expat[>=2.1.0]   [[ description = [ For DNGConverter: XMP SDK need Expat library to compile ] ]]
        dev-libs/libxml2:2.0[>=2.7.0]
        dev-libs/libxslt[>=1.1.0]
        graphics/exiv2[>=0.26]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VERSION}]   [[ note = [ desktop integration ] ]]
        kde-frameworks/kio:5[>=${KF5_MIN_VERSION}]  [[ note = [ desktop integration ] ]]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/solid:5[>=${KF5_MIN_VERSION}]
        kde-frameworks/threadweaver:5[>=${KF5_MIN_VERSION}] [[ note = [ Panorama tool ] ]]
        media-libs/lcms2[>=2.1]
        media-libs/lensfun[>=0.2.6] [[ description = [ To support LensCorrection editor ] ]]
        media-libs/liblqr:1[>=0.4.1] [[ description = [ For Liquid Rescale tool ] ]]
        media-libs/libpng:=[>=1.2.7]
        media-libs/opencv[>=3.1.0][qt5(-)]
        media-libs/tiff[>=3.8.2]
        sci-libs/eigen:3
        sys-libs/libgomp:=
        x11-dri/mesa[>=11.0]       [[ description = [ For Presentation tool ] ]]
        x11-libs/libX11[>=1.1.5]    [[ description = [ For Monitor profiles ] ]]
        x11-libs/qtbase:5[>=${QT5_MIN_VERSION}][gui][sql][sqlite]
        x11-libs/qtwebengine:5[>=${QT5_MIN_VERSION}]
        x11-libs/qtx11extras:5[>=${QT5_MIN_VERSION}] [[ description = [ For monitor profiles management with lcms ] ]]
        x11-libs/qtxmlpatterns:5[>=${QT5_MIN_VERSION}]

        calendar? ( kde-frameworks/kcalendarcore:5[>=5.63.0] )
        contacts? (
            kde-frameworks/akonadi-contact:5
            kde-frameworks/kcontacts:5
        )
        geolocation? (
            kde/marble:4
            kde-frameworks/kitemmodels:5
            kde-frameworks/kbookmarks:5
        )
        gphoto2? (
            media-libs/libgphoto2[>=2.4.0]
            virtual/usb:1
        )
        jpeg2000? ( media-libs/jasper[>=1.7.0] )
        media-player? (
            media/ffmpeg
            media-libs/qtav[>=1.12.0]
        )
        mysql? ( virtual/mysql )
        providers:graphicsmagick? ( media-gfx/GraphicsMagick[imagemagick][>=1.0.6] )
        providers:imagemagick? ( media-gfx/ImageMagick[>=5.5.4] )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        scanner? ( kde/libksane:5 )
        semantic-desktop? ( kde-frameworks/kfilemetadata:5[>=${KF5_MIN_VERSION}] )
    run:
        media-gfx/enblend-enfuse[>=3.0]
    suggestion:
        media-gfx/hugin[>=0.8] [[ description = [ Combine images to panorama views ] ]]
        kde/ffmpegthumbs:4 [[ description = [ For thumbnails of videos ] ]]
"
# NOTE: digikam unfortunately bundles libpgf-7.15.32

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DENABLE_DBUS:BOOL=TRUE
    -DENABLE_INTERNALMYSQL:BOOL=FALSE
    # Prefer QtWebEngine over unmaintained QtWebKit
    -DENABLE_QWEBENGINE:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'calendar KF5CalendarCore'
    'contacts KF5AkonadiContact'
    'doc Doxygen'
    'geolocation Marble'
    'gphoto2 Gphoto2'
    'jpeg2000 Jasper'
    'media-player QtAV'
    'scanner KF5Sane'
    'semantic-desktop KF5FileMetaData'
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'contacts AKONADICONTACTSUPPORT'
    'mysql MYSQLSUPPORT'
    'semantic-desktop KFILEMETADATASUPPORT'
)

src_compile() {
    default

    option doc && emake doc
}

src_install() {
    cmake_src_install

    if option doc; then
        insinto /usr/share/doc/${PNVR}
        doins -r api/html
    fi
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

