# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2012 Friedrich Kröner <friedrich@mailstation.de>
# Copyright 2016-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir=${PN} ] kde freedesktop-desktop gtk-icon-cache

export_exlib_phases src_configure pkg_postinst pkg_postrm

SUMMARY="Media player with support for digital tv (DVB-C/S/S2/T and ATSC, decryption with CI/CAM)"
HOMEPAGE="https://kaffeine.kde.org"

LICENCES="GPL-2 FDL-1.3"
SLOT="0"
MYOPTIONS="
    dvb [[ description = [ Support for digital television ] ]]
    ( linguas:
        ar be bg bs ca ca@valencia cs da de el en_GB eo es et eu fi fr ga gl hr hu ia id it ja km
        ko ku lt mai mr nb nds nl nn pa pl pt pt_BR ro ru se sk sl sq sr sr@ijekavian
        sr@ijekavianlatin sr@latin sv th tr ug zh_CN zh_TW
    )
"

KF5_MIN_VER="5.11.0"
QT5_MIN_VER="5.4.0"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        media/vlc[>=1.2]
        x11-libs/libX11
        x11-libs/libXScrnSaver
        x11-libs/qtbase:5[>=${QT5_MIN_VER}][gui][sql][sqlite]
        x11-libs/qtx11extras:5[>=${QT5_MIN_VER}]
        dvb? (
            media-libs/v4l-utils
            media/vlc[dvb]
        )
    run:
        dvb? ( app-text/iso-codes )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_TOOLS:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

kaffeine_src_configure() {
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DHAVE_DVB:BOOL=$(option dvb TRUE FALSE)
    )

    cmake_src_configure
}

kaffeine_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kaffeine_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

