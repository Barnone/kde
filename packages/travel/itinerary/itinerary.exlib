# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="Itinerary and boarding pass management application"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    networkmanager [[
        description = [ Defer map downloads when connected to metered network ]
    ]]
"

KF5_MIN_VER="5.64.0"
QT_MIN_VER="5.14.0"

DEPENDENCIES="
    build+run:
        dev-libs/kosmindoormap
        kde/kitinerary[>=20.04.41] [[ note = [ aka 5.14.41 ] ]]
        kde/kpkpass[>=18.11.80]
        kde-frameworks/kcontacts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kholidays:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        kde/kpublictransport
        sys-libs/zlib
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtlocation:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=1.0]
        networkmanager? ( kde-frameworks/networkmanager-qt:5[>=${KF5_MIN_VER}] )
    run:
        dev-libs/kopeninghours
        kde-frameworks/prison:5
        kde-frameworks/solid:5 [[ note =
            [ Used for controlling the screen brightness ]
        ]]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'networkmanager NetworkManagerQt'
)

itinerary_src_prepare() {
    kde_src_prepare

    # Disable test which wants to access the systemd dbus
    edo sed -e "/weathertest/d" -i autotests/CMakeLists.txt
}

itinerary_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

itinerary_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

