# Copyright 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks [ docs=false ] kde gtk-icon-cache
require python [ blacklist=2 multibuild=false ]

export_exlib_phases src_install

SUMMARY="Breeze icon theme"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] || ( LGPL-2.1 LGPL-3 )"
MYOPTIONS="
    binary-resource [[ description = [ Build a binary Qt resource containing the icons ] ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxml2:2.0 [[ note = xmllint ]]
    dev-python/lxml[python_abis:*(-)?]
    build+run:
        x11-libs/qtbase:5[>=${QT_MIN_VER}] [[
            note = [ Actually build+test but would need cmake changes ]
        ]]
        !kde/breeze:4[<5.4.90] [[
            description = [ This package has been split out from kde/breeze:4 ]
            resolution = uninstall-blocked-after
        ]]
    test:
        app-misc/fdupes[>=1.51|=1.6.1]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPython_EXECUTABLE:PATH=${PYTHON}
    # Not required, but strongly recommended upstream
    -DWITH_ICON_GENERATION:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS+=( 'binary-resource BINARY_ICONS_RESOURCE' )

breeze-icons_src_install() {
    cmake_src_install

    edo rmdir "${IMAGE}"/usr/share/icons/breeze{,-dark}/{animations,emotes}/24
}

