# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='ki18n' ] test-dbus-daemon

SUMMARY="Network transparent access to files and data"
DESCRIPTION="
This framework implements almost all the file management functions you
will ever need. In fact, the KDE file manager (Dolphin) and the KDE
file dialog also uses this to provide its network-enabled file management.

It supports accessing files locally as well as via HTTP and FTP out of the
box and can be extended by plugins to support other protocols as well. There
is a variety of plugins available, e.g. to support access via SSH.

The framework can also be used to bridge a native protocol to a file-based
interface. This makes the data accessible in all applications using the KDE
file dialog or any other KIO enabled infrastructure."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2 LGPL-2.1"
MYOPTIONS="
    acl
    designer [[ description = [ Install Qt designer plugins ] ]]
    doc [[ description = [ Build the help kioslave and documentation ] ]]
    kerberos
    X
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build+run:
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}] [[ note = [ optional ] ]]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}] [[ note = [ optional ] ]]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        sys-libs/zlib
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}] [[ note = [ Could be optional ] ]]
        acl? (
            sys-apps/acl
            sys-apps/attr
        )
        designer? ( x11-libs/qttools:5[>=${QT_MIN_VER}] )
        doc? (
            kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}] [[
                note = [ links to KF5::DocTools ]
            ]]
        )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        X? (
            x11-libs/libX11
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        )
        !kde/kio-extras:4[<5.0.95-r1] [[
            description = [ kio_trash was moved from kio-extras to kio ]
            resolution = uninstall-blocked-after
        ]]
    post:
        kde-frameworks/kded:5 [[ note = [ calls org.kde.kded5 via dbus ] ]]
    suggestion:
        kde/kde-cli-tools:4 [[
            description = [ Needed to launch various KC(ontrol)M(odule)s ]
        ]]
"

# Many tests need a running X server and a few are currently broken (5.0.0)
# Would need wsgidav gem
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Needed for tests, but also needs the ftpd gem, which is unpackaged
    -DCMAKE_DISABLE_FIND_PACKAGE_RubyExe:BOOL=TRUE
    # If a use case for this comes up we might bind it to a presumed option.
    -DKIOCORE_ONLY:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'designer DESIGNERPLUGIN'
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    ACL
    'doc KF5DocTools'
    'kerberos GSSAPI'
    'X X11'
)

