Title: kde-frameworks/libmm-qt has been renamed to modemmanager-qt
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2015-04-11
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde-frameworks/libmm-qt

Please install kde-frameworks/modemmanager-qt and *afterwards* uninstall
kde-frameworks/libmm-qt.

1. Take note of any packages depending on libmm-qt:
cave resolve \!kde-frameworks/libmm-qt

2. Install kde-frameworks/modemmanager-qt:
cave resolve modemmanager-qt -x

3. Re-install the packages from step 1.

4. Uninstall kde-frameworks/libmm-qt
cave resolve \!kde-frameworks/libmm-qt -x

Do it in *this* order or you'll potentially *break* your system.
